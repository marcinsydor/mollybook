library MollybookMain;

import 'dart:html' as html;
import 'package:stagexl/stagexl.dart';
import 'mollybookassetsLib.dart' as lib;

class MollybookMain
{
  Stage stage;
  RenderLoop renderLoop;
  lib.MollybookMain exportRoot;

  MollybookMain() {
    stage = new Stage("canvas", html.document.querySelector("#canvas"), 1024, 768, 24);

    renderLoop = new RenderLoop();
    renderLoop.addStage(stage);
  
    exportRoot = new lib.MollybookMain();

    stage.addChild(exportRoot);
  }
}
