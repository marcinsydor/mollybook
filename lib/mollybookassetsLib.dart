library MollybookMainLib;

/* WARNING: code generated using the Dart Toolkit for Adobe Flash Professional - do not edit */

import 'package:stagexl/stagexl.dart';
import 'mollybook.dart' as mollybook;

/* STAGE CONTENT */

class MollybookMain extends mollybook.Main {
	MyLabelButton button3;
	MyLabelButton button2;
	MyLabelButton button1;
	MyLabelButton button0;
	Shape shape;
	Page0 page0;
	Page1 page1;
	Page2 page2;
	Page3 page3;
	Shape shape_1;

	MollybookMain() {
		// nav
		button3 = new MyLabelButton()
		..name = "button3"
		..setTransform(328,676,1,1,0,0,0,25,25);

		button2 = new MyLabelButton()
		..name = "button2"
		..setTransform(261,676,1,1,0,0,0,25,25);

		button1 = new MyLabelButton()
		..name = "button1"
		..setTransform(192,676,1,1,0,0,0,25,25);

		button0 = new MyLabelButton()
		..name = "button0"
		..setTransform(125,676,1,1,0,0,0,25,25);

		shape = _draw(392.5,674)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AkwjlIJhAAIAAHLIphAAg").shape;

		// Layer 7
		page0 = new Page0()
		..name = "page0"
		..setTransform(592.4,336.9,1,1,0,0,0,341.4,272.9);

		// page1
		page1 = new Page1()
		..name = "page1"
		..setTransform(592.4,336.9,1,1,0,0,0,341.4,272.9);

		// page2
		page2 = new Page2()
		..name = "page2"
		..setTransform(592.4,336.9,1,1,0,0,0,341.4,272.9);

		// page3
		page3 = new Page3()
		..name = "page3"
		..setTransform(592.4,336.9,1,1,0,0,0,341.4,272.9);

		// bg
		shape_1 = _draw(512,384)
		.f(0xFFFFFFFF).s().p("EhP/A7/MAAAh3+MCf/AAAMAAAB3+g").shape;

		addChild(shape_1);
		addChild(page3);
		addChild(page2);
		addChild(page1);
		addChild(page0);
		addChild(shape);
		addChild(button0);
		addChild(button1);
		addChild(button2);
		addChild(button3);

		init();
	}
}


/* LIBRARY */

class Page3 extends mollybook.Page {
	TextField text;
	Shape shape;
	Shape shape_1;

	Page3() {
		// Layer 2
		text = new TextField("Page 3", 
		  new TextFormat("Times", 23, 0, leading:2, leftMargin:2))
		..name = "text"
		..setTransform(60,59.8)
		..width = 201
		..height = 129
		..multiline = true
		..wordWrap = true;

		// Layer 1
		shape = _draw(341.5,273)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Eg1VgqoMBqrAAAMAAABVRMhqrAAAg").shape;

		shape_1 = _draw(341.5,273)
		.f(0xFF99CC99).s().p("Eg1VAqpMAAAhVRMBqrAAAMAAABVRg").shape;

		addChild(shape_1);
		addChild(shape);
		addChild(text);

		init();
	}
}

class Page2 extends mollybook.Page {
	TextField text;
	Shape shape;
	Shape shape_1;

	Page2() {
		// Layer 2
		text = new TextField("Page 2", 
		  new TextFormat("Times", 23, 0, leading:2, leftMargin:2))
		..name = "text"
		..setTransform(60,59.8)
		..width = 201
		..height = 129
		..multiline = true
		..wordWrap = true;

		// Layer 1
		shape = _draw(341.5,273)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Eg1VgqoMBqrAAAMAAABVRMhqrAAAg").shape;

		shape_1 = _draw(341.5,273)
		.f(0xFFFF99FF).s().p("Eg1VAqpMAAAhVRMBqrAAAMAAABVRg").shape;

		addChild(shape_1);
		addChild(shape);
		addChild(text);

		init();
	}
}

class Page1 extends mollybook.Page1 {
	TextField text;
	Shape shape;
	Shape shape_1;

	Page1() {
		// Layer 2
		text = new TextField("Page 1", 
		  new TextFormat("Times", 23, 0, leading:2, leftMargin:2))
		..name = "text"
		..setTransform(60,59.8)
		..width = 201
		..height = 129
		..multiline = true
		..wordWrap = true;

		// Layer 1
		shape = _draw(341.5,273)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Eg1VgqoMBqrAAAMAAABVRMhqrAAAg").shape;

		shape_1 = _draw(341.5,273)
		.f(0xFF66FFFF).s().p("Eg1VAqpMAAAhVRMBqrAAAMAAABVRg").shape;

		addChild(shape_1);
		addChild(shape);
		addChild(text);

		init();
	}
}

class MyLabelButton extends mollybook.LabelButton {
	TextField labelTF;
	Shape shape;
	Shape shape_1;
	Shape shape_2;
	Shape shape_3;

	MyLabelButton([String mode, int startPosition, bool loop])
			: super(mode, startPosition, loop, {}) {
		// Layer 2
		labelTF = new TextField("0", 
		  new TextFormat("Times", 40, 0xFFFFFF, align:"center", leading:2, leftMargin:2))
		..name = "labelTF"
		..setTransform(8,3.8)
		..width = 31
		..height = 44
		..multiline = true
		..wordWrap = true;

		this.timeline.addTween(_tween(this.labelTF).wait(4));

		// Layer 1
		shape = _draw(25,25)
		.f(0xFF0099CC).s().p("Aj5D6IAAnzIHzAAIAAHzg").shape;

		shape_1 = _draw(25,25)
		.f(0xFF01759C).s().p("Aj5D6IAAnzIHzAAIAAHzg").shape;

		shape_2 = _draw(25,25)
		.f(0xFF005A78).s().p("Aj5D6IAAnzIHzAAIAAHzg").shape;

		shape_3 = _draw(25,25)
		.f(0xFFBDBDBD).s().p("Aj5D6IAAnzIHzAAIAAHzg").shape;

		this.timeline.addTween(_tween({}).to({"state":[{"t":this.shape}]}).to({"state":[{"t":this.shape_1}]},1).to({"state":[{"t":this.shape_2}]},1).to({"state":[{"t":this.shape_3}]},1).wait(1));

		init();
	}
}

class Circle extends MovieClip {
	Shape shape;
	Shape shape_1;
	Shape shape_2;
	Shape shape_3;
	Shape shape_4;
	Shape shape_5;
	Shape shape_6;
	Shape shape_7;
	Shape shape_8;
	Shape shape_9;
	Shape shape_10;
	Shape shape_11;
	Shape shape_12;
	Shape shape_13;
	Shape shape_14;
	Shape shape_15;
	Shape shape_16;
	Shape shape_17;
	Shape shape_18;
	Shape shape_19;
	Shape shape_20;
	Shape shape_21;
	Shape shape_22;
	Shape shape_23;
	Shape shape_24;
	Shape shape_25;
	Shape shape_26;
	Shape shape_27;
	Shape shape_28;
	Shape shape_29;
	Shape shape_30;
	Shape shape_31;
	Shape shape_32;
	Shape shape_33;
	Shape shape_34;
	Shape shape_35;
	Shape shape_36;
	Shape shape_37;
	Shape shape_38;
	Shape shape_39;
	Shape shape_40;
	Shape shape_41;
	Shape shape_42;
	Shape shape_43;
	Shape shape_44;
	Shape shape_45;
	Shape shape_46;
	Shape shape_47;
	Shape shape_48;
	Shape shape_49;
	Shape shape_50;
	Shape shape_51;
	Shape shape_52;
	Shape shape_53;
	Shape shape_54;
	Shape shape_55;
	Shape shape_56;
	Shape shape_57;
	Shape shape_58;
	Shape shape_59;

	Circle([String mode, int startPosition, bool loop])
			: super(mode, startPosition, loop, {}) {
		// Layer 1
		shape = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AH+AAQAADTiWCVQiVCWjTAAQjSAAiWiWQiViVAAjTQAAjSCViWQCWiVDSAAQDTAACVCVQCWCWAADSg").shape;

		shape_1 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AlnFoQiViVgBjTQABjSCViVQCViVDSgBQDTABCVCVQCVCVAADSQAADTiVCVQiVCVjTAAQjSAAiViVg").shape;

		shape_2 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AofAAQAAjgCfigQCgifDgAAQDhAACfCfQCgCgAADgQAADhigCfQifCgjhAAQjgAAigigQififAAjhg").shape;

		shape_3 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AmAGAQififAAjhQAAjgCfigQCgifDgAAQDhAACfCfQCgCgAADgQAADhigCfQifCgjhAAQjgAAigigg").shape;

		shape_4 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("ApBAAQAAjuCpiqQCqipDuAAQDvAACqCpQCpCqAADuQAADvipCqQiqCpjvAAQjuAAiqipQipiqAAjvg").shape;

		shape_5 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AmYGZQipiqAAjvQAAjuCpiqQCqipDuAAQDvAACqCpQCpCqAADuQAADvipCqQiqCpjvAAQjuAAiqipg").shape;

		shape_6 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("ApkAAQAAj9C0izQCzi0D9AAQD+AACzC0QC0CzAAD9QAAD+i0CzQizC0j+AAQj9AAizi0Qi0izAAj+g").shape;

		shape_7 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AmwGxQizizAAj+QAAj9CzizQCzizD9AAQD+AACzCzQCzCzABD9QgBD+izCzQizCzj+ABQj9gBizizg").shape;

		shape_8 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AqGAAQAAkLC9i+QC+i9ELAAQEMAAC9C9QC+C+AAELQAAEMi+C9Qi9C+kMAAQkLAAi+i+Qi9i9AAkMg").shape;

		shape_9 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AnJHJQi9i9AAkMQAAkLC9i+QC+i9ELAAQEMAAC9C9QC+C+AAELQAAEMi+C9Qi9C+kMAAQkLAAi+i+g").shape;

		shape_10 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AqoAAQAAkZDHjIQDIjHEZAAQEaAADHDHQDIDIAAEZQAAEajIDHQjHDIkaAAQkZAAjIjIQjHjHAAkag").shape;

		shape_11 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AnhHhQjHjHAAkaQAAkZDHjIQDIjHEZAAQEaAADHDHQDIDIAAEZQAAEajIDHQjHDIkaAAQkZAAjIjIg").shape;

		shape_12 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("ArLAAQAAknDSjSQDSjSEnAAQEoAADSDSQDSDSAAEnQAAEojSDSQjSDSkoAAQknAAjSjSQjSjSAAkog").shape;

		shape_13 = _draw(51,51)
		.f(0xFFFF99CC).s().p("An5H6QjSjSAAkoQAAknDSjSQDSjSEnAAQEoAADSDSQDRDSAAEnQAAEojRDSQjSDRkoAAQknAAjSjRg").shape;

		shape_14 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("ArtAAQAAk2DbjcQDcjbE2AAQE3AADbDbQDcDcAAE2QAAE3jcDbQjbDck3AAQk2AAjcjcQjbjbAAk3g").shape;

		shape_15 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AoSISQjbjbAAk3QAAk2DbjcQDcjbE2AAQE3AADbDbQDcDcAAE2QAAE3jcDbQjbDck3AAQk2AAjcjcg").shape;

		shape_16 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AsPAAQAAlEDljmQDmjlFEAAQFFAADlDlQDmDmAAFEQAAFFjmDlQjlDmlFAAQlEAAjmjmQjljlAAlFg").shape;

		shape_17 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AoqIqQjljlAAlFQAAlEDljmQDmjlFEAAQFFAADlDlQDmDmAAFEQAAFFjmDlQjlDmlFAAQlEAAjmjmg").shape;

		shape_18 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AsyAAQAAlSDwjwQDwjwFSAAQFTAADvDwQDxDwAAFSQAAFTjxDvQjvDxlTAAQlSAAjwjxQjwjvAAlTg").shape;

		shape_19 = _draw(51,51)
		.f(0xFFFF99CC).s().p("ApCJCQjvjvAAlTQAAlSDvjwQDwjvFSAAQFTAADvDvQDxDwAAFSQAAFTjxDvQjvDxlTAAQlSAAjwjxg").shape;

		shape_20 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AtUAAQAAlgD6j6QD6j6FgAAQFhAAD6D6QD6D6AAFgQAAFhj6D6Qj6D6lhAAQlgAAj6j6Qj6j6AAlhg").shape;

		shape_21 = _draw(51,51)
		.f(0xFFFF99CC).s().p("ApaJbQj6j6AAlhQAAlgD6j6QD6j6FgAAQFhAAD6D6QD6D6AAFgQAAFhj6D6Qj6D6lhAAQlgAAj6j6g").shape;

		shape_22 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("At2AAQAAlvEDkEQEEkDFvAAQFwAAEDEDQEEEEAAFvQAAFwkEEDQkDEElwAAQlvAAkEkEQkDkDAAlwg").shape;

		shape_23 = _draw(51,51)
		.f(0xFFFF99CC).s().p("ApzJzQkDkDAAlwQAAlvEDkEQEEkDFvAAQFwAAEDEDQEEEEAAFvQAAFwkEEDQkDEElwAAQlvAAkEkEg").shape;

		shape_24 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AuYAAQAAl9ENkOQEOkNF9AAQF+AAENENQEOEOAAF9QAAF+kOENQkNEOl+AAQl9AAkOkOQkNkNAAl+g").shape;

		shape_25 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AqLKLQkOkNAAl+QAAl9EOkOQEOkOF9AAQF+AAENEOQEOEOAAF9QAAF+kOENQkNEOl+AAQl9AAkOkOg").shape;

		shape_26 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Au7AAQAAmLEYkYQEYkYGLAAQGMAAEXEYQEZEYAAGLQAAGMkZEXQkXEZmMAAQmLAAkYkZQkYkXAAmMg").shape;

		shape_27 = _draw(51,51)
		.f(0xFFFF99CC).s().p("AqjKjQkYkXAAmMQAAmLEYkYQEYkYGLAAQGMAAEXEYQEZEYAAGLQAAGMkZEXQkXEZmMAAQmLAAkYkZg").shape;

		shape_28 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AvdAAQAAmZEhkjQEjkhGZAAQGaAAEiEhQEiEjAAGZQAAGakiEiQkiEimaAAQmZAAkjkiQkhkiAAmag").shape;

		shape_29 = _draw(51,51)
		.f(0xFFFF99CC).s().p("Aq8K8QkhkiAAmaQAAmZEhkjQEjkhGZAAQGaAAEiEhQEiEjAAGZQAAGakiEiQkiEimaAAQmZAAkjkig").shape;

		shape_30 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AQBAAQAAGpktErQkrEtmpAAQmoAAksktQkskrAAmpQAAmoEsksQEsksGoAAQGpAAErEsQEtEsAAGog").shape;

		shape_31 = _draw(51,51)
		.f(0xFFFF99CC).s().p("ArULUQksksABmoQgBmoEsksQEsksGoABQGogBEsEsQEtEsAAGoQAAGoktEsQksEtmoAAQmoAAksktg").shape;

		shape_32 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AvbAAQAAmYEhkiQEikhGYAAQGZAAEhEhQEiEiAAGYQAAGZkiEhQkhEimZAAQmYAAkikiQkhkhAAmZg").shape;

		shape_33 = _draw(51,51)
		.f(0xFFF49DD0).s().p("Aq6K6QkhkhAAmZQAAmYEhkiQEikhGYAAQGZAAEhEhQEiEiAAGYQAAGZkiEhQkhEimZAAQmYAAkikig").shape;

		shape_34 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Au2AAQAAmJEWkXQEXkWGJAAQGKAAEWEWQEXEXAAGJQAAGKkXEWQkWEXmKAAQmJAAkXkXQkWkWAAmKg").shape;

		shape_35 = _draw(51,51)
		.f(0xFFE9A0D3).s().p("AqgKgQkWkWAAmKQAAmJEWkXQEXkWGJAAQGKAAEWEWQEXEXAAGJQAAGKkXEWQkWEXmKAAQmJAAkXkXg").shape;

		shape_36 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AuRAAQAAl6ELkMQEMkLF6AAQF7AAELELQEMEMAAF6QAAF7kMELQkLEMl7AAQl6AAkMkMQkLkLAAl7g").shape;

		shape_37 = _draw(51,51)
		.f(0xFFDEA4D7).s().p("AqGKGQkLkLAAl7QAAl6ELkMQEMkLF6AAQF7AAELELQEMEMAAF6QAAF7kMELQkLEMl7AAQl6AAkMkMg").shape;

		shape_38 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AtsAAQAAlrEAkBQEBkAFrAAQFsAAEAEAQEBEBAAFrQAAFskBEAQkAEBlsAAQlrAAkBkBQkAkAAAlsg").shape;

		shape_39 = _draw(51,51)
		.f(0xFFD3A8DB).s().p("ApsJsQkAkAAAlsQAAlrEAkBQEBkAFrAAQFsAAEAEAQEBEBAAFrQAAFskBEAQkAEBlsAAQlrAAkBkBg").shape;

		shape_40 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AtIAAQAAlbD2j3QD3j2FbAAQFcAAD2D2QD3D3AAFbQAAFcj3D2Qj2D3lcAAQlbAAj3j3Qj2j2AAlcg").shape;

		shape_41 = _draw(51,51)
		.f(0xFFC8ABDE).s().p("ApSJSQj2j2AAlcQAAlbD2j3QD3j2FbAAQFcAAD2D2QD3D3AAFbQAAFcj3D2Qj2D3lcAAQlbAAj3j3g").shape;

		shape_42 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AsjAAQAAlMDrjsQDsjrFMAAQFNAADrDrQDsDsAAFMQAAFNjsDrQjrDslNAAQlMAAjsjsQjrjrAAlNg").shape;

		shape_43 = _draw(51,51)
		.f(0xFFBDAFE2).s().p("Ao4I4QjrjrAAlNQAAlMDrjsQDsjrFMAAQFNAADrDrQDsDsAAFMQAAFNjsDrQjrDslNAAQlMAAjsjsg").shape;

		shape_44 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Ar+AAQAAk9DgjhQDhjgE9AAQE+AADgDgQDhDhAAE9QAAE+jhDgQjgDhk+AAQk9AAjhjhQjgjgAAk+g").shape;

		shape_45 = _draw(51,51)
		.f(0xFFB3B3E6).s().p("AoeIeQjgjgAAk+QAAk9DgjhQDhjgE9AAQE+AADgDgQDhDhAAE9QAAE+jhDgQjgDhk+AAQk9AAjhjhg").shape;

		shape_46 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("ArZAAQAAkuDVjWQDWjVEuAAQEvAADVDVQDWDWAAEuQAAEvjWDVQjVDWkvAAQkuAAjWjWQjVjVAAkvg").shape;

		shape_47 = _draw(51,51)
		.f(0xFFA8B6E9).s().p("AoEIEQjVjWAAkuQAAkuDVjWQDWjVEuAAQEuAADWDVQDWDWAAEuQAAEujWDWQjWDWkuAAQkuAAjWjWg").shape;

		shape_48 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Aq0AAQAAkeDKjMQDMjKEeAAQEfAADLDKQDLDMAAEeQAAEfjLDLQjLDLkfAAQkeAAjMjLQjKjLAAkfg").shape;

		shape_49 = _draw(51,51)
		.f(0xFF9DBAED).s().p("AnqHqQjKjLAAkfQAAkeDKjMQDMjKEeAAQEfAADLDKQDLDMAAEeQAAEfjLDLQjLDLkfAAQkeAAjMjLg").shape;

		shape_50 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AqQAAQAAkPDAjBQDBjAEPAAQEQAADADAQDBDBAAEPQAAEQjBDAQjADBkQAAQkPAAjBjBQjAjAAAkQg").shape;

		shape_51 = _draw(51,51)
		.f(0xFF92BDF0).s().p("AnQHQQjAjAAAkQQAAkPDAjBQDBjAEPAAQEQAADADAQDBDBAAEPQAAEQjBDAQjADBkQAAQkPAAjBjBg").shape;

		shape_52 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AprAAQAAkAC1i2QC2i1EAAAQEBAAC1C1QC2C2AAEAQAAEBi2C1Qi1C2kBAAQkAAAi2i2Qi1i1AAkBg").shape;

		shape_53 = _draw(51,51)
		.f(0xFF87C1F4).s().p("Am2G2Qi1i1AAkBQAAkAC1i2QC2i1EAAAQEBAAC1C1QC2C2AAEAQAAEBi2C1Qi1C2kBAAQkAAAi2i2g").shape;

		shape_54 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("ApGAAQAAjxCqirQCriqDxAAQDyAACqCqQCrCrAADxQAADyirCqQiqCrjyAAQjxAAirirQiqiqAAjyg").shape;

		shape_55 = _draw(51,51)
		.f(0xFF7CC5F8).s().p("AmcGcQiqirAAjxQAAjwCqisQCsiqDwAAQDxAACrCqQCrCsAADwQAADxirCrQirCrjxAAQjwAAisirg").shape;

		shape_56 = _draw(51,51)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("AohAAQAAjhCfihQChifDhAAQDiAACgCfQCgChAADhQAADiigCgQigCgjiAAQjhAAihigQifigAAjig").shape;

		shape_57 = _draw(51,51)
		.f(0xFF71C8FB).s().p("AmCGCQifigAAjiQAAjhCfihQChifDhAAQDiAACgCfQCgChAADhQAADiigCgQigCgjiAAQjhAAihigg").shape;

		shape_58 = _draw(51,51)
		.f().s(0xFF000000).ss(1,1,1).p("AH+AAQAADTiWCVQiVCWjTAAQjSAAiWiWQiViVAAjTQAAjSCViWQCWiVDSAAQDTAACVCVQCWCWAADSg").shape;

		shape_59 = _draw(51,51)
		.f(0xFF66CCFF).s().p("AlnFoQiViVgBjTQABjSCViVQCViVDSgBQDTABCVCVQCVCVAADSQAADTiVCVQiVCVjTAAQjSAAiViVg").shape;

		this.timeline.addTween(_tween({}).to({"state":[{"t":this.shape_1},{"t":this.shape}]}).to({"state":[{"t":this.shape_3},{"t":this.shape_2}]},1).to({"state":[{"t":this.shape_5},{"t":this.shape_4}]},1).to({"state":[{"t":this.shape_7},{"t":this.shape_6}]},1).to({"state":[{"t":this.shape_9},{"t":this.shape_8}]},1).to({"state":[{"t":this.shape_11},{"t":this.shape_10}]},1).to({"state":[{"t":this.shape_13},{"t":this.shape_12}]},1).to({"state":[{"t":this.shape_15},{"t":this.shape_14}]},1).to({"state":[{"t":this.shape_17},{"t":this.shape_16}]},1).to({"state":[{"t":this.shape_19},{"t":this.shape_18}]},1).to({"state":[{"t":this.shape_21},{"t":this.shape_20}]},1).to({"state":[{"t":this.shape_23},{"t":this.shape_22}]},1).to({"state":[{"t":this.shape_25},{"t":this.shape_24}]},1).to({"state":[{"t":this.shape_27},{"t":this.shape_26}]},1).to({"state":[{"t":this.shape_29},{"t":this.shape_28}]},1).to({"state":[{"t":this.shape_31},{"t":this.shape_30}]},1).to({"state":[{"t":this.shape_33},{"t":this.shape_32}]},1).to({"state":[{"t":this.shape_35},{"t":this.shape_34}]},1).to({"state":[{"t":this.shape_37},{"t":this.shape_36}]},1).to({"state":[{"t":this.shape_39},{"t":this.shape_38}]},1).to({"state":[{"t":this.shape_41},{"t":this.shape_40}]},1).to({"state":[{"t":this.shape_43},{"t":this.shape_42}]},1).to({"state":[{"t":this.shape_45},{"t":this.shape_44}]},1).to({"state":[{"t":this.shape_47},{"t":this.shape_46}]},1).to({"state":[{"t":this.shape_49},{"t":this.shape_48}]},1).to({"state":[{"t":this.shape_51},{"t":this.shape_50}]},1).to({"state":[{"t":this.shape_53},{"t":this.shape_52}]},1).to({"state":[{"t":this.shape_55},{"t":this.shape_54}]},1).to({"state":[{"t":this.shape_57},{"t":this.shape_56}]},1).to({"state":[{"t":this.shape_59},{"t":this.shape_58}]},1).wait(1));

	}
}

class Page0 extends mollybook.Page0 {
	TextField text;
	Circle circle;
	Shape shape;
	Shape shape_1;

	Page0() {
		// Layer 2
		text = new TextField("Page 0", 
		  new TextFormat("Times", 23, 0, leading:2, leftMargin:2))
		..name = "text"
		..setTransform(60,59.8)
		..width = 201
		..height = 129
		..multiline = true
		..wordWrap = true;

		// Layer 3
		circle = new Circle()
		..name = "circle"
		..setTransform(544,116.5,1,1,0,0,0,51,51);

		// Layer 1
		shape = _draw(341.5,273)
		.f().s(0xFFFFFFFF).ss(1,1,1).p("Eg1VgqoMBqrAAAMAAABVRMhqrAAAg").shape;

		shape_1 = _draw(341.5,273)
		.f(0xFFCCCC33).s().p("Eg1VAqpMAAAhVRMBqrAAAMAAABVRg").shape;

		addChild(shape_1);
		addChild(shape);
		addChild(circle);
		addChild(text);

		init();
	}
}



/* SHORTCUTS */

const _tween = TimelineTween.get;
const _ease = TransitionFunction.custom;
const _draw = _ShapeFactory.create;

class _ShapeFactory {
  Shape _shape;
  Graphics _graphics;
  Function _endFill;
  Function _endStroke;
  num _strokeWidth = 1;
  String _strokeJoints = "miter";
  String _strokeCaps = "butt";
  
  static _ShapeFactory create(num x, num y) {
    return new _ShapeFactory(x, y);
  }
  
  Shape get shape {
    ef(); es();
    return _shape;
  }
  Graphics get graphics {
    ef(); es();
    return _graphics;
  }
  
  _ShapeFactory(num x, num y) {
    _shape = new Shape();
    _graphics = _shape.graphics;
    _shape.x = x.toDouble();
    _shape.y = y.toDouble();
  }
  
  _ShapeFactory beginLinearGradientFill(List<int> colors, List<num> stops, num x0, num y0, num x1, num y1) {
    return lf(colors, stops, x0, y0, x1, y1);
  }
  _ShapeFactory beginRadialGradientFill(List<int> colors, List<num> stops, num x0, num y0, num r0, num x1, num y1, num r1) {
    return rf(colors, stops, x0, y0, r0, x1, y1, r1);
  }
  _ShapeFactory beginBitmapFill(Bitmap image, [String repeat, Matrix mat]) {
    return bf(image, repeat, mat);
  }
  _ShapeFactory beginFill([int color]) {
    return f(color);
  }
  _ShapeFactory endFill() {
    return ef();
  }
  _ShapeFactory beginStroke([int color]) {
    return s(color);
  }
  _ShapeFactory beginLinearGradientStroke(List<int> colors, List<num> stops, num x0, num y0, num x1, num y1) {
    return ls(colors, stops, x0, y0, x1, y1);
  }
  _ShapeFactory beginRadialGradientStroke(List<int> colors, List<num> stops, num x0, num y0, num r0, num x1, num y1, num r1) {
    return rs(colors, stops, x0, y0, r0, x1, y1, r1);
  }
  _ShapeFactory setStrokeStyle(num thickness, [caps, joints, num miterLimit=10, bool ignoreScale=false]) {
    return ss(thickness, caps, joints, miterLimit, ignoreScale);
  }
  _ShapeFactory closePath() {
    return cp();
  }
  
  _ShapeFactory moveTo(num x, num y) {
    _graphics.moveTo(x, y);
    return this;
  }
  _ShapeFactory lineTo(num x, num y) {
    _graphics.lineTo(x, y);
    return this;
  }
  _ShapeFactory curveTo(num cx, num cy, num x, num y) {
    _graphics.quadraticCurveTo(cx, cy, x, y);
    return this;
  }
  _ShapeFactory drawCircle(num x, num y, num radius) {
    return dc(x, y, radius);
  }
  _ShapeFactory drawEllipse(num x, num y, num width, num height) {
    return de(x, y, width, height);
  }
  _ShapeFactory drawRect(num x, num y, num width, num height) {
    return dr(x, y, width, height);
  }
  _ShapeFactory drawRectRounded(num x, num y, num width, num height, num radius) {
    return rr(x, y, width, height, radius);
  }
  _ShapeFactory drawPolyStar (num x, num y, num radius, num sides, num pointSize, num angle) {
    return dp(x, y, radius, sides, pointSize, angle);
  }
  
  // fills
  _ShapeFactory lf(List<int> colors, List<num> stops, num x0, num y0, num x1, num y1) {
    ef();
    var gradient = new GraphicsGradient.linear(x0, y0, x1, y1);
    int n = colors.length;
    for(int i = 0; i<n; i++) {
      gradient.addColorStop(stops[i], colors[i]);
    }
    _endFill = () {
      _graphics.fillGradient(gradient);
    };
    return this;
  }
  _ShapeFactory rf(List<int> colors, List<num> stops, num x0, num y0, num r0, num x1, num y1, num r1) {
    ef();
    var gradient = new GraphicsGradient.radial(x0, y0, r0, x1, y1, r1);
    int n = colors.length;
    for(int i = 0; i<n; i++) {
      gradient.addColorStop(stops[i], colors[i]);
    }
    _endFill = () {
      _graphics.fillGradient(gradient);
    };
    return this;
  }
  _ShapeFactory bf(Bitmap image, [String repeat, Matrix mat]) {
    var bmp = image.bitmapData;
    GraphicsPattern pattern = null;
    if (mat != null) mat.translate(-_shape.x, -_shape.y);
    else mat = new Matrix(1, 0, 0, 1, -_shape.x, -_shape.y);
    switch (repeat) {
      case "repeat-x": pattern = new GraphicsPattern.repeatX(bmp, mat); break;
      case "repeat-y": pattern = new GraphicsPattern.repeatY(bmp, mat); break;
      case "no-repeat": pattern = new GraphicsPattern.noRepeat(bmp, mat); break;
      default: pattern = new GraphicsPattern.repeat(bmp, mat); break;
    }
    _endFill = () {
      _graphics.fillPattern(pattern);
    };
    return this;
  }
  _ShapeFactory f([int color]) {
    if (color == null) color = 0;
    ef();
    _endFill = () {
      _graphics.fillColor(color);
    };
    return this;
  }
  _ShapeFactory ef() {
    if (_endFill != null) {
      _endFill();
      _endFill = null;
    }
    return this;
  }
  
  // stroke
  _ShapeFactory s([int color]) {
    es();
    if (color == null) color = 0;
    _endStroke = () {
      _graphics.strokeColor(color, _strokeWidth, _strokeJoints, _strokeCaps);
    };
    _graphics.beginPath();
    return this;
  }
  _ShapeFactory ls(List<int> colors, List<num> stops, num x0, num y0, num x1, num y1) {
    es();
    var gradient = new GraphicsGradient.linear(x0, y0, x1, y1);
    int n = colors.length;
    for(int i = 0; i<n; i++) {
      gradient.addColorStop(stops[i], colors[i]);
    }
    _endStroke = () {
      _graphics.strokeGradient(gradient, _strokeWidth, _strokeJoints, _strokeCaps);
    };
    return this;
  }
  _ShapeFactory rs(List<int> colors, List<num> stops, num x0, num y0, num r0, num x1, num y1, num r1) {
    es();
    var gradient = new GraphicsGradient.radial(x0, y0, r0, x1, y1, r1);
    int n = colors.length;
    for(int i = 0; i<n; i++) {
      gradient.addColorStop(stops[i], colors[i]);
    }
    _endStroke = () {
      _graphics.strokeGradient(gradient, _strokeWidth, _strokeJoints, _strokeCaps);
    };
    return this;
  }
  _ShapeFactory es() {
    if (_endStroke != null) {
      _endStroke();
      _endStroke = null;
    }
    return this;
  }
  _ShapeFactory ss(num thickness, [caps, joints, num miterLimit=10, bool ignoreScale=false]) {
    _strokeWidth = thickness;
    if (caps != null) {
      switch(caps) {
        case 0: _strokeCaps = "butt"; break;
        case 1: _strokeCaps = "round"; break;
        case 2: _strokeCaps = "square"; break;
        default: _strokeCaps = caps.toString(); break;
      }
    }
    else _strokeCaps = "butt";
    if (joints != null) {
      switch(joints) {
        case 0: _strokeJoints = "miter"; break;
        case 1: _strokeJoints = "round"; break;
        case 2: _strokeJoints = "bevel"; break;
        default: _strokeJoints = joints.toString(); break;
      }
    }
    else _strokeJoints = "miter";
    return this;
  }
  
  _ShapeFactory cp() {
    _graphics.closePath();
    return this;
  }
  _ShapeFactory mt(num x, num y) {
    _graphics.moveTo(x, y);
    return this;
  }
  _ShapeFactory lt(num x, num y) {
    _graphics.lineTo(x, y);
    return this;
  }
  _ShapeFactory c(num cx, num cy, num x, num y) {
    _graphics.quadraticCurveTo(cx, cy, x, y);
    return this;
  }
  _ShapeFactory dc(num x, num y, num radius) {
    _graphics.circle(x, y, radius);
    return this;
  }
  _ShapeFactory de(num x, num y, num width, num height) {
    _graphics.ellipse(x, y, width, height);
    return this;
  }
  _ShapeFactory dr(num x, num y, num width, num height) {
    _graphics.rect(x, y, width, height);
    return this;
  }
  _ShapeFactory rr(num x, num y, num width, num height, num radius) {
    _graphics.rectRound(x, y, width, height, radius, radius);
    return this;
  }
  _ShapeFactory dp(num x, num y, num radius, num sides, num pointSize, num angle) {
    // TODO PolyStar
    return this;
  }
  
  // decodePath
  _ShapeFactory p(String str) {
    _graphics.decode(str);
    return this;
  }
}
