library mollybook;

import 'package:stagexl/stagexl.dart';

part 'src/main.dart';
part 'src/page.dart';
part 'src/page0.dart';
part 'src/page1.dart';
part 'src/page2.dart';
part 'src/page3.dart';
part 'src/button.dart';
part 'src/labelbutton.dart';