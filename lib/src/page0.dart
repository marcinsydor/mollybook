part of mollybook;

class Page0 extends Page {
  
  MovieClip circle;
  
  EventStreamSubscription circleSubscription;
  
  init() {
    
    print(circle);
    
    circle.stop();
    
  }
  
  void open() {
    
    super.open();
    
    circle.gotoAndPlay(0);
    
    circleSubscription = circle.onEnterFrame.listen((e) {
      if (circle.currentFrame == circle.totalFrames - 1) {
        circle.stop();
      }
    });
    
  }
  
  void close() {
    
    super.close();
    
    circleSubscription.cancel();
    
  }
  
}