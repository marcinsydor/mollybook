part of mollybook;

class Main extends MovieClip {
    
  int _numPages = 4;
  
  
  List<LabelButton> buttons = [];
  List<Page> pages = [];
  
  int _currentPageIndex;
  
  init() {

    for (var i = 0; i < _numPages; i++) {
      
      var button = getChildByName('button${i}') as LabelButton
          ..label = '${i + 1}'
          ..onMouseClick.listen((e) {
            var n = e.target.name.replaceFirst('button', '');
            _openPage(int.parse(n));
          });
      buttons.add(button);
      
      var page = getChildByName('page${i}') as Page;
      pages.add(page);

    }

    
    _openPage(0);
  }
  
  void _openPage(int pageIndex) {
    
    if (_currentPageIndex != null) {
      pages[_currentPageIndex].close();
      buttons[_currentPageIndex].enabled = true;
    }
    
    pages[pageIndex].open();
    buttons[pageIndex].enabled = false;
    
    _currentPageIndex = pageIndex;
    
    
  }
  
}