part of mollybook;

class LabelButton extends Button {
  
  TextField labelTF;

  LabelButton([String mode, int startPosition, bool loop, Map<String, num> labels])
  : super(mode, startPosition, loop, {}) {
      
  }
  
  String get label => labelTF.text; 
  set label(String value) => labelTF.text = value; 
  
  
  void init() {
     super.init();
  }
  
}