part of mollybook;

class Button extends MovieClip {
  
  bool _enabled = true;
  
  MovieClip target;
  
  
  Button([String mode, int startPosition, bool loop, Map<String, num> labels])
  : super(mode, startPosition, loop, {});
    

  bool get enabled => _enabled;
  set enabled(bool value) {
    _enabled = value;
    mouseEnabled = value;
    buttonMode = value;
    gotoAndStop(value ? 0 : 3);
  }
  
  
  void init() {
    
    useHandCursor = true;
    mouseChildren = false;

    stop();
    addEventListener(MouseEvent.MOUSE_OVER, _onMouseEvent);
    addEventListener(MouseEvent.MOUSE_OUT, _onMouseEvent);
    addEventListener(MouseEvent.MOUSE_DOWN, _onMouseEvent);
    addEventListener(MouseEvent.MOUSE_UP, _onMouseEvent);
    
  
    
  }
  
  //-------------------------------------------------------------------------------------------------
  //-------------------------------------------------------------------------------------------------

  void _onMouseEvent(MouseEvent mouseEvent) {
    
    if (!enabled) return;
    
    if (mouseEvent.type == MouseEvent.MOUSE_OUT) {
      gotoAndStop(0);
    } else {
      gotoAndStop(mouseEvent.buttonDown ? 2 : 1);
    }
  }

  

  
}


